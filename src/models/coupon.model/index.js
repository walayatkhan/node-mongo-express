const mongoose = require("mongoose");

const CouponSchema = new mongoose.Schema(
  {
    type: { type: String },
    code: { type: String },
    details: { type: Array },
    discount: { type: Number },
    discount_type: { type: String },
    start_date: { type: Date },
    end_date: { type: Date },
    status: { type: Boolean },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Coupons", CouponSchema);
