const mongoose = require("mongoose");

const AppSettingSchema = new mongoose.Schema({
  key: { type: String },
  value: { type: String },
  status: { type: Boolean },
});

module.exports = mongoose.model("AppSettings", AppSettingSchema);
