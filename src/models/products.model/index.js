const { Decimal128 } = require("bson");
const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema(
  {
    name: { type: String },
    category_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Categories",
      required: true,
    },
    brand: { type: mongoose.Schema.Types.ObjectId, ref: "Brands" },
    images: { type: Array },
    video_link: { type: String },
    tags: { type: String },
    description: { type: String },
    unit_price: { type: Decimal128 },
    purchase_price: { type: Decimal128 },
    varients: { type: Boolean },
    attributes: { type: Array },
    colors: { type: Array },
    published: { type: Boolean },
    quantity: { type: Number },
    payment_type: { type: String },
    featured: { type: Boolean },
    discount: { type: Number },
    tax: { type: Decimal128 },
    shipping_type: { type: String },
    shipping_cost: { type: Decimal128 },
    number_sale: { type: Number },
    meta_title: { type: String },
    slug: { type: String },
    rating: { type: Number },
    barcode: { type: String },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Products", ProductSchema);
