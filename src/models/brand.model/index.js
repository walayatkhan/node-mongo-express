const mongoose = require("mongoose");

const BrandSchema = new mongoose.Schema(
  {
    name: { type: String },
    slug: { type: String },
    priority: { type: String },
    logo: { type: String },
    description: { type: String },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Brands", BrandSchema);
