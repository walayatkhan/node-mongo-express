const mongoose = require("mongoose");

const BannerSchema = new mongoose.Schema(
  {
    image: { type: String },
    url: { type: String },
    position: { type: Number },
    status: { type: Boolean },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Banners", BannerSchema);
