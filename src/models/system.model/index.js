const mongoose = require("mongoose");

const SystemSettingSchema = new mongoose.Schema(
  {
    name: { type: String },
    abbr: { type: String },
    slug: { type: String },
    logo: { type: String },
    header_logo: { type: String },
    footer_logo: { type: String },
    copy_right: { type: String },
    contact: { type: String },
    phone: { type: String },
    email: { type: String },
    currency_id: { type: String },
    youtube: { type: String },
    facebook: { type: String },
    instagram: { type: String },
    twitter: { type: String },
    color_code: { type: String },
    language_id: { type: String },
    description: { type: String },
    address: { type: String },
    favicon: { type: String },
  },
  { timestamps: true }
);

module.exports = mongoose.model("SystemSetting", SystemSettingSchema);
