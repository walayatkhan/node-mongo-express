const { Double, Decimal128 } = require("bson");
const mongoose = require("mongoose");

let UserSchema = new mongoose.Schema(
  {
    first_name: { type: String },
    last_name: { type: String },
    email: {
      type: String,
      trim: true,
      unique: true,
      required: "email Address is Required",
    },
    email_verified_at: { type: Date },
    email_verify_code: { type: String },
    contact_no: { type: String },
    phone: { type: String },
    password: { type: String },
    remember_me: { type: Boolean },
    type: { type: String },
    image: { type: String },
    balance: { type: Decimal128 },
    status: { type: Boolean },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Users", UserSchema);
