const mongoose = require("mongoose");

const CouponUsageSchema = new mongoose.Schema(
  {
    users: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
      required: true,
    },
    coupons: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Coupons",
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("CouponUsages", CouponUsageSchema);
