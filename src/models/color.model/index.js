const mongoose = require("mongoose");

const ColorSchema = new mongoose.Schema(
  {
    name: { type: String },
    code: { type: String },
    status: { type: Boolean },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Colors", ColorSchema);
