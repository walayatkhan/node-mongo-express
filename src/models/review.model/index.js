const mongoose = require("mongoose");

const ReviewSchema = new mongoose.Schema(
  {
    product: { type: mongoose.Schema.Types.ObjectId, ref: "Products" },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
    rating: { type: Number },
    comment: { type: String },
    status: { type: Boolean },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Reviews", ReviewSchema);
