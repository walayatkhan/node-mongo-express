const mongoose = require("mongoose");

const AddressSchema = new mongoose.Schema(
  {
    house: { type: String },
    street: { type: String },
    address: { type: String },
    city: { type: String },
    country: { type: String },
    contact: { type: String },
    phone: { type: String },
    post_code: { type: Number },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Addresses", AddressSchema);
