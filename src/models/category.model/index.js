const mongoose = require("mongoose");

const CategorySchema = new mongoose.Schema({
  parent_id: { type: mongoose.Schema.Types.ObjectId, ref: "Categories" },
  order_level: { type: Number },
  name: { type: String },
  slug: { type: String },
  meta_title: { type: String },
  description: { type: String },
  image: { type: String },
  icon: { type: String },
  featured: { type: Boolean },
  digital: { type: Boolean },
});

module.exports = mongoose.model("Categories", CategorySchema);
