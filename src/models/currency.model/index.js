const mongoose = require("mongoose");

const CurrencySchema = new mongoose.Schema(
  {
    name: { type: String },
    symbol: { type: String },
    exchange_rate: { type: Number },
    status: { type: Boolean },
    code: { type: String },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Currencies", CurrencySchema);
