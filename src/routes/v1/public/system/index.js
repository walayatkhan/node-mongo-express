const Router = require("express").Router();
const { system } = require("../../../../controllers");

Router.route("/").get(system.getAll).post(system.addOne);
Router.route("/:id")
  .delete(system.deleteOne)
  .get(system.getOne)
  .put(system.updateOne);

module.exports = Router;
