const Router = require("express").Router();
const loginRoutes = require("./login");
const addressRoutes = require("./address");
const appRoutes = require("./app");
const bannerRoutes = require("./banner");
const brandRoutes = require("./brand");
const categoryRoutes = require("./category");
const colorRoutes = require("./color");
const couponUsageRoutes = require("./coupon_usage");
const couponRoutes = require("./coupon");
const currencyRoutes = require("./currency");
const productRoutes = require("./products");
const reviewRoutes = require("./review");
const systemRoutes = require("./system");
const publicRoutes = [
  {
    path: "/user",
    route: loginRoutes,
  },
  {
    path: "/address",
    route: addressRoutes,
  },
  {
    path: "/app",
    route: appRoutes,
  },
  {
    path: "/banner",
    route: bannerRoutes,
  },
  {
    path: "/brand",
    route: brandRoutes,
  },
  {
    path: "/category",
    route: categoryRoutes,
  },
  {
    path: "/color",
    route: colorRoutes,
  },
  {
    path: "/coupon_usage",
    route: couponUsageRoutes,
  },
  {
    path: "/coupon",
    route: couponRoutes,
  },
  {
    path: "/currency",
    route: currencyRoutes,
  },
  {
    path: "/product",
    route: productRoutes,
  },
  {
    path: "/review",
    route: reviewRoutes,
  },
  {
    path: "/system",
    route: systemRoutes,
  },
];

module.exports = { publicRoutes };
