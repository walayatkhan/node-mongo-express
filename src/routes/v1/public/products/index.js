const Router = require("express").Router();
const { products } = require("../../../../controllers");

Router.route("/").get(products.getAll).post(products.addOne);
Router.route("/:id")
  .delete(products.deleteOne)
  .get(products.getOne)
  .put(products.updateOne);

module.exports = Router;
