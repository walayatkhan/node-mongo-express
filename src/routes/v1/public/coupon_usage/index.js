const Router = require("express").Router();
const { coupon_usage } = require("../../../../controllers");

Router.route("/").get(coupon_usage.getAll).post(coupon_usage.addOne);
Router.route("/:id")
  .delete(coupon_usage.deleteOne)
  .get(coupon_usage.getOne)
  .put(coupon_usage.updateOne);

module.exports = Router;
