const Router = require("express").Router();
const { currency } = require("../../../../controllers");

Router.route("/").get(currency.getAll).post(currency.addOne);
Router.route("/:id")
  .delete(currency.deleteOne)
  .get(currency.getOne)
  .put(currency.updateOne);

module.exports = Router;
