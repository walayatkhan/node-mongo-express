const Router = require("express").Router();
const { review } = require("../../../../controllers");

Router.route("/").get(review.getAll).post(review.addOne);
Router.route("/:id")
  .delete(review.deleteOne)
  .get(review.getOne)
  .put(review.updateOne);

module.exports = Router;
