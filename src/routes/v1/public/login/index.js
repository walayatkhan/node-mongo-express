const Router = require("express").Router();
const { user } = require("../../../../controllers");

Router.route("/").get(user.getAll).post(user.addOne);
Router.route("/:id")
  .delete(user.deleteOne)
  .get(user.getOne)
  .put(user.updateOne);

module.exports = Router;
