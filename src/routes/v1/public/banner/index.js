const Router = require("express").Router();
const { banner } = require("../../../../controllers");

Router.route("/").get(banner.getAll).post(banner.addOne);
Router.route("/:id")
  .delete(banner.deleteOne)
  .get(banner.getOne)
  .put(banner.updateOne);

module.exports = Router;
