const Router = require("express").Router();
const { category } = require("../../../../controllers");

Router.route("/").get(category.getAll).post(category.addOne);
Router.route("/:id")
  .delete(category.deleteOne)
  .get(category.getOne)
  .put(category.updateOne);

module.exports = Router;
