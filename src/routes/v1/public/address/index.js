const Router = require("express").Router();
const { address } = require("../../../../controllers");

Router.route("/").get(address.getAll).post(address.addOne);
Router.route("/:id")
  .delete(address.deleteOne)
  .get(address.getOne)
  .put(address.updateOne);

module.exports = Router;
