const Router = require("express").Router();
const { brand } = require("../../../../controllers");

Router.route("/").get(brand.getAll).post(brand.addOne);
Router.route("/:id")
  .delete(brand.deleteOne)
  .get(brand.getOne)
  .put(brand.updateOne);

module.exports = Router;
