const Router = require("express").Router();
const { color } = require("../../../../controllers");

Router.route("/").get(color.getAll).post(color.addOne);
Router.route("/:id")
  .delete(color.deleteOne)
  .get(color.getOne)
  .put(color.updateOne);

module.exports = Router;
