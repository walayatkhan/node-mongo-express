const Router = require("express").Router();
const { coupon } = require("../../../../controllers");

Router.route("/").get(coupon.getAll).post(coupon.addOne);
Router.route("/:id")
  .delete(coupon.deleteOne)
  .get(coupon.getOne)
  .put(coupon.updateOne);

module.exports = Router;
