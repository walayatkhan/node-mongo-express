const Router = require("express").Router();
const { app } = require("../../../../controllers");

Router.route("/").get(app.getAll).post(app.addOne);
Router.route("/:id").delete(app.deleteOne).get(app.getOne).put(app.updateOne);

module.exports = Router;
