const { reviewModel } = require("../../models");

const getAll = async (req, res) => {
  try {
    let data = await reviewModel
      .find()
      .populate("product")
      .populate("user")
      .exec();
    res.send({ data: data });
  } catch (e) {
    res.send({ error: e, status: 404 });
  }
};

const addOne = async (req, res) => {
  try {
    let add = await reviewModel(req.body).save();
    res.send({ data: add, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};
const updateOne = async (req, res) => {
  try {
    let add = await reviewModel(req.body).save();
    res.send({ data: add, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

const deleteOne = async (req, res) => {
  try {
    let result = await reviewModel.deleteOne({ _id: req.params.id });
    res.send({ data: result, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

const getOne = async (req, res) => {
  try {
    let result = await reviewModel
      .findOne({ _id: req.params.id })
      .populate("product")
      .populate("user")
      .exec();
    if (result) res.send({ data: result, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

module.exports = { getAll, addOne, deleteOne, updateOne, getOne };
