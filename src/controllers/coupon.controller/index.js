const { couponModel } = require("../../models");

const getAll = async (req, res) => {
  try {
    let data = await couponModel.find();
    res.send({ data: data });
  } catch (e) {
    res.send({ error: e, status: 404 });
  }
};

const addOne = async (req, res) => {
  try {
    let add = await couponModel(req.body).save();
    res.send({ data: add, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};
const updateOne = async (req, res) => {
  try {
    let add = await couponModel(req.body).save();
    res.send({ data: add, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

const deleteOne = async (req, res) => {
  try {
    let result = await couponModel.deleteOne({ _id: req.params.id });
    res.send({ data: result, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

const getOne = async (req, res) => {
  try {
    let result = await couponModel.findOne({ _id: req.params.id });
    if (result) res.send({ data: result, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

module.exports = { getAll, addOne, deleteOne, updateOne, getOne };
