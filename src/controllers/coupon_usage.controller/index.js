const { couponUsageModel } = require("../../models");

const getAll = async (req, res) => {
  try {
    let data = await couponUsageModel
      .find()
      .populate("users")
      .populate("coupons")
      .exec();
    res.send({ data: data });
  } catch (e) {
    res.send({ error: e, status: 404 });
  }
};

const addOne = async (req, res) => {
  try {
    let add = await couponUsageModel(req.body).save();
    res.send({ data: add, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};
const updateOne = async (req, res) => {
  try {
    let add = await couponUsageModel(req.body).save();
    res.send({ data: add, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

const deleteOne = async (req, res) => {
  try {
    let result = await couponUsageModel.deleteOne({ _id: req.params.id });
    res.send({ data: result, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

const getOne = async (req, res) => {
  try {
    let result = await couponUsageModel
      .findOne({ _id: req.params.id })
      .populate("users")
      .populate("coupons")
      .exec();
    if (result) res.send({ data: result, status: 200 });
  } catch (e) {
    res.send({ error: e, status: 422 });
  }
};

module.exports = { getAll, addOne, deleteOne, updateOne, getOne };
